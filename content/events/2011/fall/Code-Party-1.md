---
name: 'Code Party 1'
short: '*by Calum T. Dalek*. The Computer Science Club is having our first code party of the term! The theme for this code party will be collaborative development. We''ll present several ideas of small projects to work on for the unexperienced. Everyone is welcome; please bring your friends! There will be foodstuffs and sugary drinks available for your hacking pleasure.'
startDate: 'September 30 2011 19:00'
online: false
location: 'Comfy Lounge'
---

The Computer Science Club is having our first code party of the term! The theme for this code party will be collaborative development. We'll present several ideas of small projects to work on for the unexperienced. Everyone is welcome; please bring your friends! There will be foodstuffs and sugary drinks available for your hacking pleasure.

There will be 4 more code parties this term.

