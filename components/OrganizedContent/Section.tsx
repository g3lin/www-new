import { MDXRemote, MDXRemoteSerializeResult } from "next-mdx-remote";
import React from "react";

import { createLink, OrganizedContent } from "@/components/OrganizedContent";

import { Title } from "../Title";

import { Header } from "./Header";
import { Options } from "./ReadAll";

interface Section {
  id: string;
  title: string;
}

export interface Props {
  content: MDXRemoteSerializeResult;
  sections: Section[];
  current: number;
}

export function createSectionPage({
  title,
  image,
  pagePath,
  getShapesConfig,
  link,
  description,
  imagePosition,
  numberedSections,
}: Options) {
  const Link = link ?? createLink(pagePath);

  function Page(this: void, { content, sections, current }: Props) {
    return (
      <>
        <Title>{[sections[current].title, title]}</Title>
        <Header
          title={title}
          image={image}
          description={description}
          imagePosition={imagePosition}
        >
          <OrganizedContent
            sections={sections}
            id={sections[current].id}
            pageTitle={title}
            link={Link}
            numberedSections={numberedSections}
          >
            <MDXRemote {...content} />
          </OrganizedContent>
        </Header>
      </>
    );
  }

  Page.getShapesConfig = getShapesConfig;

  return Page;
}
