## Social Advice

#### What is an affordable way to watch movies with friends?

If you’re looking to watch movies with friends then you can either buy cheaper (Tuesday prices) at the Student Life Center or Waterloo has a list of streaming sites where you can watch free movies.

#### How can I become more involved on campus?

Join different clubs or societies! They’re a great way to make friends and manage your time better. Plus, it makes going through a school term much more fun.

#### How do I build my social skills and get out of my comfort zone?

Take up the opportunities for meeting people. You never know who you might meet. If you don’t put yourself out there and take chances, it’s much harder to find a relationship, friendships, or even study buddies.

#### What can I do to prevent being affected by the competitive environment?

Be kind. Celebrate your friends’ successes when they get a co-op job and support them when they’re struggling too. Waterloo is so competitive and sometimes it can be hard to navigate through, so make sure you’re giving and getting a good support network.

## Additional Resources

#### Where can I find the free movie database?

Along with your tuition fees, part of your library fees grant you access to a database of [free movies](https://media3-criterionpic-com.proxy.lib.uwaterloo.ca/htbin/wwform/006/wwk770?&kw=zkcode%7C000005%7C)

#### What are some links to SE discord servers?

SE servers:

- [discord.gg/ZtmRPc59](https://discord.gg/ZtmRPc59)
- [discord.gg/XyQtsfe5](https://discord.gg/XyQtsfe5)

#### What are some resources for interview prep?

- Group Leetcode server:
[discord.gg/kwCsCNb3](https://discord.gg/kwCsCNb3)

There are many online resources for interview preparation including https://evykassirer.github.io/playing-the-internship-game/ and https://github.com/viraptor/reverse-interview

- Internship/Interview advice
https://www.techintern.io/ 

#### Where can I access eBooks?

https://subjectguides.uwaterloo.ca/compsci/books

More specifically O'Reilly Higher education: https://learning-oreilly-com.proxy.lib.uwaterloo.ca/home
There are a lot of helpful books/videos that can teach you a variety of things from finance to leadership to a variety of cs topics! (With recommendations, case studies and playlist to help you get started)

#### What is the link to the GPUs?

We have GPUs: https://uwaterloo.ca/math-faculty-computing-facility/services/service-catalogue-teaching-linux/access-teaching-gpu-cluster

#### Where can I find all of the math faculty services?

https://uwaterloo.ca/math-faculty-computing-facility/services