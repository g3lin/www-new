---
name: "Cali Panel"
short: "Are you struggling with finding a work-life balance? Or interested in how to build your career in the tech industry? Come to the Cali Panel!"
startDate: "July 23 2022 18:30"
endDate: "July 23 2022 20:30"
online: true
location: "Online"
poster: "images/events/2022/spring/Cali-Panel.png"
registerLink: "https://forms.gle/eZRruDdWhYtuGmj3A"
---

📣 Are you struggling with finding a work-life balance? Or interested in how to build your career in the tech industry? Come to the Cali Panel!

🚀 CSC is bringing together current and past students working for prestigious tech companies to talk about a variety of topics including career, networking, work-life balance, travelling for work, etc.

🤩 You will be able to get some insights into how to achieve your dream careers while having a work-life balance! Check out our panelists below.

📅 Event date: July 23rd, 6:30 PM - 8:30 PM.

📍This is an online event. Zoom link: https://us06web.zoom.us/j/86890664040?pwd=T3RxWFFXeTlQTER4L2hEaGl6Q3E0Zz09

👉 Sign up from this link: https://forms.gle/eZRruDdWhYtuGmj3A
