---
author: 'shahanneda'
date: 'Mar 10 2023 00:00'
---
🔊 Calling all CS/CFM/CS-BBA candidates for the graduating class of 2023!

🥳 You are invited to participate in CSC’s 2023 Class Profile!
Congratulations for making it to the final lap of your degree! Whether you're on a co-op or academic term, we know you’ve worked hard to get here and probably have many thoughts on your experience as a CS/CFM/CS-BBA student at University of Waterloo. The 2023 class profile is an opportunity for you to share your experiences and learn more about your graduating class.

🤔 What is the class profile?
It’s an ANONYMOUS survey that asks questions about your experience as a CS/CFM/CS-BBA student at the University of Waterloo. After the data collection, we will release a summary of the responses so you can learn more about your graduating class. To learn more about the end goal please take a look at the first ever CS class profile (https://csclub.uwaterloo.ca/classprofile/2022/) released for last years graduating class.

⏳ When is it due?
For your responses to be recorded and to be eligible for the giveaway, the form will be due on April 30th, 2023.

👀 Why should I fill it out?
CSC has always created an inclusive community for CS students to voice their opinions and experiences and help out students by keeping them involved and informed. The first CS class profile was released for last year and it was a huge success benefiting many students. This is your chance to share your exhilarating experiences by participating in the CS class profile. The information will not only give you insight into your peer group but also serve as a guide for current and prospective CS students.After filling out the form, you will be entered into a raffle to win 1 of 3 Amazon gift cards.

⚡ ️Prizes:
1 x $50 Amazon Gift Card
2 x $25 Amazon Gift Card

We can’t wait to learn more about the amazing class of 2023! ✨
Here is the link to fill out the survey: http://csclub.ca/2023-cs-class-profile-survey, feel free to share it with any of your 2023 graduating class friends!
