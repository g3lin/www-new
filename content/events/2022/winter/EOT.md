---
name: 'CSC EOT Social Night'
short: 'CSC is hosting a fun night of activities to celebrate the end of the term, with scavenger hunts, trivia, paint night sessions, board games, charades and more! '
startDate: 'April 03 2022 18:00'
online: false
location: 'DC 1351'
poster: 'images/events/2022/winter/EOT.png'
registerLink: https://bit.ly/w22-eot-signup
---

 📢 Phew, what a term! CSC is hosting a fun night of activities to celebrate the end of the term, with scavenger hunts, trivia, paint night sessions, board games, charades and more! 

🎉 Come join us on Sunday April 3rd, from 6-7PM for the Scavenger Hunt, and 7-9PM for our night of activities!

✨Some reminders before attending our event:
1. Registration is required to attend our in-person event - register at https://bit.ly/w22-eot-signup!
2. Please remember to keep face coverings on at all times when you’re indoors.
3. There are limited quantities for paint supplies! First come, first serve 🎨
4. Make sure to bring your WATcard in order to win prizes! Only those that have paid MathSoc fees in their tuition will be able to receive the prizes for activities.

🥳 This event is limited to those with a CSC membership. If you’d like to sign up for one, visit https://csclub.uwaterloo.ca/get-involved/ or come with your watcard on the day of to sign up for FREE!

📆 Event Date: April 3rd at 6-9PM ET at DC 1351

👉 Register at https://bit.ly/w22-eot-signup. Alternatively, you can email us at exec@csclub.uwaterloo.ca to sign up.