---
name: 'Eclipse'
short: 'How I Stopped Worrying and Learned to Love the IDE'
startDate: 'November 24 2004 16:30'
online: false
location: 'MC 2066'
---

How I stopped worrying and Learned to Love the IDE

Audience: anyone who as ever used the Java programming language to do anything. Especially if you don't like the IDEs you've seen so far or still use (g)Vi(m) or (X)Emacs.

I'll go through some of the coolest features of the best IDE (which stands for "IDEs Don't Eat" or "Integrated Development Environment") I've seen. For the first year and seasoned almost-grad alike!

