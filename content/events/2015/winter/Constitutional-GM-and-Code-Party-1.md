---
name: 'Constitutional GM and Code Party 1'
short: 'GM for the W2015 term, two main amendments to be discussed: Requiring elections to be held within two weeks of the beginning of term and adopting a club-wide code of conduct. <br> Code Party 1 follows, we''re doing timed code golf problems, T-shirts might find themselves on people who do well on code golf.'
startDate: 'March 27 2015 18:00'
online: false
location: 'EIT 1015'
---

GM for the W2015 term, two main amendments to be discussed: Requiring elections to be held within two weeks of the beginning of term and adopting a club-wide code of conduct. 


 Code Party 1 follows, we're doing timed code golf problems, T-shirts might find themselves on people who do well on code golf.

