---
title: User Responsibilities
---

Users must be responsible for their behaviour. Users, and not the CSC, will be held accountable for any of their illegal, damaging or unethical actions. Such actions are obviously not permitted on CSC machines.

Users must act responsibly, and the needs of others with regard to computing resources must be considered at all times. In particular, no user should use any resource to an extent that other users are prevented from similarly using that resource, and no user's actions shall disrupt the work of other users.

Users must also abide by the usage policies of all machines that they connect to from CSC machines, or use to connect to CSC machines. It is the users' responsibility to check the policies of *all* machines that they connect to.
