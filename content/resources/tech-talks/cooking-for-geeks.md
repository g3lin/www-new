---
index: 27
title: 'Cooking for Geeks'
presentors:
  - Jeff Potter
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/cooking-for-geeks-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/cooking-for-geeks-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/cooking-for-geeks.mp4'
    type: 'Talk (x264)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/cooking-for-geeks.mpg'
    type: 'Talk (MPG)'
---

The CSC is happy to be hosting Jeff Potter, author of "Cooking for Geeks" for a presentation on the finer arts of food science. Jeff's book has been featured on NPR, BBC and his presentations have wowed audiences of hackers & foodies alike. We're happy to have Jeff joining us for a hands on demonstration.

But you don't have to take our word for it... here's what Jeff has to say:

Hi! I'm Jeff Potter, author of Cooking for Geeks (O'Reilly Media, 2010), and I'm doing a "D.I.Y. Book Tour" to talk about my just-released book. I'll talk about the food science behind what makes things yummy, giving you a quick primer on how to go into the kitchen and have a fun time turning out a good meal. Depending upon the space, I’ll also bring along some equipment or food that we can experiment with, and give you a chance to play with stuff and pester me with questions.
