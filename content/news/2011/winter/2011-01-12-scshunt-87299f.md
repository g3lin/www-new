---
author: 'scshunt'
date: 'January 12 2011 00:00'
---

There is a correction in the nominee list. m4burns was nominated for Vice-President, not b4taylor. Apologies. If you cannot attend the meeting, please send a vote to cro@csclub.uwaterloo.ca.