---
name: "🏖️ Tropical 🏖️ Code Party"
short: "Come join us for our 🏖️ Tropical 🏖️ themed code party! There will be free food, fun games, and much more!"
startDate: "June 8 2023 19:00"
endDate: "June 8 2023 21:00"
online: false
location: "AHS 1689"
poster: "images/events/2023/spring/tropical-code-party.png"
---

📣 📣 Attention CSC members! Come join us for our 🏖️ Tropical 🏖️ themed code party! There will be free food, fun games, and much more 👀 So come hang out, study, and have a refreshing time with us!

📆 Event Date: Thursday June 8th, 7-9pm

📌 Location: AHS 1689

See y'all there ❤️
