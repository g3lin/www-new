---
name: 'Semacode: Image recognition on mobile camera phones'
short: 'Simon Woodside, founder of Semacode, comes to discuss image what it is like to start a business and how imaging code works'
startDate: 'July 19 2006 16:30'
online: false
location: 'MC1085'
---

Could you write a good image recognizer for a 100 MHz mobile phone processor with 1 MB heap, 320x240 image, on a poorly-optimized Java stack? It needs to locate and read two-dimensional barcodes made up of square modules which might be no more than a few pixels in size. We had to do that in order to establish Semacode, a local start up company that makes a software barcode reader for cell phones. The applications vary from ubiquitous computing to advertising. Simon Woodside (founder) will discuss what it's like to start a business and how the imaging code works.

