---
name: 'Prabhakar Ragde'
short: 'Functional Lexing and Parsing'
startDate: 'March 09 2009 15:00'
online: false
location: 'DC1302'
---

This talk will describe a non-traditional functional approach	to the classical problems of lexing (breaking a stream of	characters into "words" or tokens) and parsing (identifying	tree structure in a stream of tokens based on a grammar,	e.g. for a programming language that needs to be compiled or	interpreted). The functional approach can clarify and organize	a number of algorithms that tend to be opaque in their	conventional imperative presentation. No prior background in	functional programming, lexing, or parsing is assumed.

