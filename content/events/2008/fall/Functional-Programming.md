---
name: 'Functional Programming'
short: 'This talk will survey concepts, techniques, and languages for functional programming from both historical and contemporary perspectives, with reference to Lisp, Scheme, ML, Haskell, and Erlang. No prior background is assumed.'
startDate: 'November 10 2008 16:30'
online: false
location: 'MC4061'
---

This talk will survey concepts, techniques, and	languages for functional programming from both historical and	contemporary perspectives, with reference to Lisp, Scheme, ML, Haskell, and Erlang. No prior background is assumed.

