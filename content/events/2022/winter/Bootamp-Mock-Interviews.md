---
name: 'Bootcamp: Mock Interviews'
short: 'Come to practice your interview skills with experienced mentors!'
startDate: 'January 23 2022 18:00'
online: true
location: 'Discord'
poster: 'images/events/2022/winter/Bootcamp-resume-review-mock-interview.png'
registerLink: https://lnkd.in/eHrs7_Fi
---

📢 Mentee applications for Bootcamp are now OPEN! 📢 CSC is bringing back Bootcamp to gear you up for your next recruiting season, partnered with the UW Tech clubs! 💻 The drop-in mock interview event takes place January 23rd 6:00 - 10:00 PM EST.

💁‍♀️ Sign up as a mentee, and join our experienced mentors in Mock Interviews (virtual 1:1 sessions) to receive feedback from various tech backgrounds 📃 We’ll be using an SE22 FYDP project called ReviewKit. You will be paired with a mentor who is knowledgeable in the same or a similar career path to yours to ensure relevant feedback! 👌

A mentor will be paired with you based on your career interests to provide insightful feedback and advice to rock your job search - don’t miss out! If you’re interested, please sign up! We would love to help you feel ready and confident for the upcoming job hunt.

📝After signing up, you’ll soon receive a link to the Discord server in which this event takes place. Our collaborating clubs are excited to bring you this opportunity to sharpen your job hunting skills

👉 If you’re interested, please apply at https://lnkd.in/eHrs7_Fi

Alternatively, you can email us at exec@csclub.uwaterloo.ca with the year and program you’re in, along with interested job paths.

📅 Deadline to Apply: January 16th, 2022, 11:59AM EST