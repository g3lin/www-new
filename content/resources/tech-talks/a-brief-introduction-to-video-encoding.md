---
index: 26
title: 'A Brief Introduction to Video Encoding'
presentors:
  - Peter Barfuss
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/pbarfuss-video-encoding-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/pbarfuss-video-encoding-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/pbarfuss-video-encoding.mp4'
    type: 'Talk (x264)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/pbarfuss-video-encoding.mpg'
    type: 'Talk (MPG)'
---

In this talk, I will go over the concepts used in video encoding (such as motion estimation/compensation, inter- and intra- frame prediction, quantization and entropy encoding), and then demonstrate these concepts and algorithms in use in the MPEG-2 and the H.264 video codecs. In addition, some clever optimization tricks using SIMD/vectorization will be covered, assuming sufficient time to cover these topics.

With the recent introduction of digital TV and the widespread success of video sharing websites such as youtube, it is clear that the task of lossily compressing video with good quality has become important. Similarly, the complex algorithms involved require high amounts of optimization in order to run fast, another important requirement for any video codec that aims to be widely used/adopted.
