---
name: 'Brush-Based Constructive Solid Geometry'
short: 'The last talk in the CS10 series will be presented by Jordan Saunders, in which he will discuss methods for processing brush-based constructive solid geometry.'
startDate: 'April 06 2010 16:30'
online: false
location: 'DC1304'
---

For some would-be graphics programmers, the biggest barrier-to-entry is getting data to render. This is why there exist so many terrain renderers: by virtue of the fact that rendering height-fields tends to give pretty pictures from next to no "created" information. However, it becomes more difficult when programmers want to do indoor rendering (in the style of the Quake and Unreal games). Ripping map information from the Quake games is possible (and fairly simple), but their tool-chain is fairly clumsy from the point of view of adding a conversion utility.

My talk is about Constructive Solid Geometry from a Brush-based perspective (nearly identical to Unreal's and still very similar to Quake's). The basic idea is that there are brushes (convex volumes in 3-space) and they can either be additive (solid brushes) or subtractive (hollow, or air brushes). The entire world starts off as an infinite solid lump and you can start removing sections of it then adding them back in. The talk pertains to fast methods of taking the list of brushes and generating world geometry. I may touch on interface problems with the editor, but the primary content will be the different ways I generated the geometry and what I found to be best.

