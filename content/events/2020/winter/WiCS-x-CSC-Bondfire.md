---
name: 'WiCS x CSC Bondfire'
short: '"Bonfire" with WiCS Thursday March 12th @ 6PM in MC Comfy'
startDate: 'March 12 2020 18:00'
online: false
location: 'MC Comfy'
---

CSC and WiCS are hosting an indoor get together event Thursday March 12th @ 6PM in MC Comfy. Gather around the "bonfire" for a night of fun, food, and friends! Free hot chocolate, food and s'mores on us!

