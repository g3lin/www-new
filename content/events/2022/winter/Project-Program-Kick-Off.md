---
name: 'Project Program Kick-Off'
short: 'Attend to get details about Project Program, a month-long event that will have mentors support your team in creating a side-project.'
startDate: 'January 28 2022 19:00'
online: true
location: 'Zoom'
poster: 'images/events/2022/winter/Project-Program-Kick-Off.png'
registerLink: https://forms.gle/b6e2vda7Y8wDfBLdA
---

📢 Project Program is back for Winter 2022, and we’re excited to see mentors support you to create a month-long project!

DSC and CSC are collaborating again to help you create your side project by guiding your group of mentees through brainstorming project ideas, creating roadmaps with milestones and achievements, and finally presenting your project for the chance to win prizes! 🏆

📌 The details of the program will be discussed during this event, so if you’re interested in participating, be sure to attend!

📅 Event Date: Friday, January 28th from 7:00-8:00pm EDT on Zoom. 💻
https://us06web.zoom.us/j/89144349767?pwd=Z3BZREI0MGNRWXdFWmZMM1JRVU5CQT09

👉  No need to register, but we'll send you an email alert if you do! Sign up at https://forms.gle/b6e2vda7Y8wDfBLdA. Alternatively, you can also email us at exec@csclub.uwaterloo.ca to sign up as well.

See you then! 👋
