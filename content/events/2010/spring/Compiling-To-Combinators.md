---
name: 'Compiling To Combinators'
short: 'Professor Ragde will be giving the first of our Professor talks for the Spring 2010 term.'
startDate: 'June 22 2010 16:30'
online: false
location: 'MC2066'
---

Number theory was thought to be mathematically appealing but practically useless until the RSA encryption algorithm demonstrated its considerable utility. I'll outline how combinatory logic (dating back to 1920) has a similarly unexpected application to efficient and effective compilation, which directly catalyzed the development of lazy functional programming languages such as Haskell. The talk is self-contained; no prior knowledge of functional programming is necessary.

