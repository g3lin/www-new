---
name: 'In the Beginning'
short: '**by Dr. Prabhakar Ragde, Cheriton School of Computer Science**. I''ll be workshopping some lecture ideas involving representations of numbers, specification of computation in functional terms, reasoning about such specifications, and comparing the strengths and weaknesses of different approaches.'
startDate: 'September 21 2010 16:30'
online: false
location: 'MC4061'
---

I'll be workshopping some lecture ideas involving representations of numbers, specification of computation in functional terms, reasoning about such specifications, and comparing the strengths and weaknesses of different approaches. No prior background is needed; the talk should be accessible to anyone attending the University of Waterloo and, I hope, interesting to both novices and experts.

