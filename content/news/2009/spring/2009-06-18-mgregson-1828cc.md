---
author: 'mgregson'
date: 'June 18 2009 01:00'
---

### Do you text... do you iPod?

We are looking for volunteers to take part in a study on mobile hand held device use and any related health or comfort troubles experienced during their use. As a participant in this study, you would be asked to fill out a 5-8 minute computer-administered confidential questionnaire.

If you would like to participate, please follow this link: [http://ithumb.iwh.on.ca/](<http://ithumb.iwh.on.ca/>) This study is being conducted by Richard Wells and Sophia Berolo, Department of Kinesiology, and Benjamin Amick, Institute for Work and Health. If you wish more information, please contact Sophia Berolo at sberolo@uwaterloo.ca. The study has been reviewed by, and received ethics clearance through, the office of Research Ethics, University of Waterloo.