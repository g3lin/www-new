---
name: 'CSC Goes To Dooly''s'
short: 'We''re going to Dooly''s to play pool. What more do you want from us? Come to the Club office and we''ll all bus there together. We''ve got discount tables for club members so be sure to be there.'
startDate: 'October 20 2009 16:30'
online: false
location: 'MC3036'
---

We're going to Dooly's to play pool. What more do you want from us? Come to the Club office and we'll all bus there together. We've got discount tables for club members so be sure to be there.

