---
name: 'InstallFest'
short: 'See [http://uw-dig.uwaterloo.ca/installfest/](<http://uw-dig.uwaterloo.ca/installfest/>)'
startDate: 'January 12 2004 15:00'
online: false
location: 'DC1301'
---

An Installfest is an opportunity to install software on your computer. People come with computers. Other people come with experience. The people get together and (when all goes well) everybody leaves satisfied.

You are invited to our first installfest of the year. Come to get some software or to learn more about Open Source Software and why it is relevant to your life. The event is free, but you may want to bring blank CDs and/or money to purchase some open source action for your computer at home.

See the [UW-DIG website](<http://uw-dig.uwaterloo.ca/installfest/>) for more details.

