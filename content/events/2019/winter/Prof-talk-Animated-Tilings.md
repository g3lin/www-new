---
name: 'Prof talk: Animated Tilings'
short: 'Professor Craig Kaplan will be talking about tiling arrangements on March 20th, 2019, at 4 PM in PHY 150'
startDate: 'March 20 2019 16:00'
online: false
location: 'PHY 150'
---

By slowly varying the shapes of tiles in a tiling, it's possible to create fun and interesting abstract animations. Many of these animations are a good fit for the genre of looping mathematical GIFs that proliferate in social media. In this talk, I'll introduce a few core concepts from tiling theory and then discuss techniques for constructing tilings that evolve in space or in time.

