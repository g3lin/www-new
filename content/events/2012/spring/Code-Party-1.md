---
name: 'Code Party 1'
short: 'The Computer Science Club is running the first code party of the term! Come join us and hack on open source software, your own projects, or whatever comes up. Everyone is welcome; please bring your friends. There will be foodstuffs and sugary drinks available for your hacking pleasure.'
startDate: 'June 08 2012 19:00'
online: false
location: 'MC 3001'
---



The Computer Science Club is running the first code party of the term! Come join us and hack on open source software, your own projects, or whatever comes up. Everyone is welcome; please bring your friends. There will be foodstuffs and sugary drinks available for your hacking pleasure.

