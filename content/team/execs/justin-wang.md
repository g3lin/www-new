---
name: Justin Wang
role: Vice-President
---

Hey there! I'm Justin and I am a 2b CO + Pmath student, and am the 2024 Spring term Vice President. Some fun facts about me:
- I played hockey since I was 7
- Enjoy sim racing, and race whenever I can
- Avid biker and sometimes swim, and have done a triathalon
- Getting into photography again, mostly taking portraits
I am always in the CSC office so don't be afraid to come by and say hi!