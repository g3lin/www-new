---
index: 3
title: 'Feminism in STEM - a 101 Panel'
presentors:
  - Prabhakar
  - Fatema
  - Filzah
  - Swetha
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/fem101-questions-thumb-small.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/fem101-panel-discussion.mp4'
    type: 'Panel questions and discussion (mp4)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/fem101-questions.mp4'
    type: 'Audience questions (mp4)'
---

A panel organized by the CS Club on how feminism manifests itself in STEM, specifically CS and Engineering.

Panelists are Dr. Prabhakar Ragde, Swetha Kulandaivelan, and Filzah Nasir. Moderated by Fatema Boxwala.

Due to battery trouble, the first few minutes of audio were lost. The panelists were introduced as Prabhakar from the School of Computer Science, Swetha from 4A Mechanical Engineering, and Filzah as an Engineering grad student.

Sample questions from the panel section are:

- Filzah and Swetha, can you expand on how Engineering tries to keep its curriculum grounded in reality?
- Why would an Engineering 101 instructor tell the class to design urinals?
- Prabhakar, how can men in STEM help women get their voices heard?

<!-- -->

 Sample questions from the audience after the panel: - As a woman in CS, how do I know I wasn't hired to meet a diversity target?
- Filzah, you mentioned that "getting to 50%" isn't what you're interested in. Can you expand on that?
- An admittedly selfish argument I've seen on Reddit asks why we should cooperate with marginalized communities when we're not significantly affected by them? (Response at 10 minutes into questions)
- Prabhakar, how has CS changed since you were an undergrad?

<!-- -->

A statistical errata: The Math faculty proportionally gives offers of admission to the 25% of women that apply, and there are no significant disproportionate dropout rates.
