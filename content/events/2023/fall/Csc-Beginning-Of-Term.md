---
name: 'CSC Beginning of Term!'
short: 'Kick off the fall term with CSC’s BOT event!'
startDate: 'September 19 2023 19:00'
endDate: 'September 19 2023 22:00'
online: false
location: 'STC 0010'
poster: 'images/events/2023/fall/1707083684169--CSC-BOT.jpg'
---

📢 Kick off the fall term with CSC’s BOT event! Are you interested in attending upcoming CSC events? Want to meet others in the CS community? Want to have fun and eat food? You don’t want to miss our first event of this term!

🎉Come join us for a night of speed-friending and fun games including IRL gartic phone, cup stacking, and extreme rock paper scissors! 

📆 When? September 19th, 2023 at 7:00 - 10:00pm EST

📍 Where? STC 0010

