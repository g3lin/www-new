---
name: 'Regular Expressions'
short: 'Find your perfect match'
startDate: 'January 23 2003 18:30'
online: false
location: 'MC1085'
---

Stephen Kleene developed regular expressions to describe what he called <q>the algebra of regular sets.</q>

 Since he was a pioneering theorist in computer science, Kleene's regular expressions soon made it into searching algorithms and from there to everyday tools.

Regular expressions can be powerful tools to manipulate text. You will be introduced to them in this talk. As well, we will go further than the rigid mathematical definition of regular expressions, and delve into POSIX regular expressions which are typically available in most Unix tools.

