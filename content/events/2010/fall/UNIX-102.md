---
name: 'UNIX 102'
short: 'This installment in the CS Club''s popular Unix tutorials UNIX 102 introduces powerful text editing tools for programming and document formatting.'
startDate: 'October 13 2010 16:30'
online: false
location: 'MC3003'
---

Unix 102 is a follow up to Unix 101, requiring basic knowledge of the shell. If you missed Unix101 but still know your way around you should be fine. Topics covered include: "real" editors, document typesetting with LaTeX (great for assignments!), bulk editing, spellchecking, and printing in the student environment and elsewhere.

