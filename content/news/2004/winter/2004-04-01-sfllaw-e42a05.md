---
author: 'sfllaw'
date: 'April 01 2004 00:00'
---

`glucose-fructose` will be going down for hardware upgrades on 2004-04-02 for an indeterminate amount of time. We thank MathSoc and MEF for their generous support in purchasing this hardware, and apologise for any inconvenience this downtime will cause.