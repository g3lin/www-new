import { createSectionPage } from "@/components/OrganizedContent/Section";
import {
  createSectionGetStaticPaths,
  createSectionGetStaticProps,
} from "@/components/OrganizedContent/static";

import { options } from "./";

export default createSectionPage(options);

export const getStaticProps = createSectionGetStaticProps(options.pagePath);
export const getStaticPaths = createSectionGetStaticPaths(options.pagePath);
