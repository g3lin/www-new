---
name: 'Afterhours'
short: 'Come join us for chill, small-group discussions about a variety of topics, including relationships and friendships, maintaining routine, dealing with imposter syndrome and burnout, and any other topics you’d like to bring into the conversation!'
startDate: 'December 3 2022 18:00'
endDate: 'December 3 2022 20:00'
online: false
location: 'SCL Multipurpose Room'
poster: 'images/events/2022/fall/Afterhours.png'
registerLink: https://forms.gle/cmNo2tbcof2UvEQ5A 
---
📣 Afterhours is back!!

😌 Come join us for chill, small-group discussions about a variety of topics, including relationships and friendships, maintaining routine, dealing with imposter syndrome and burnout, and any other topics you’d like to bring into the conversation!

🤩 As you rotate between different discussions, you’ll get the chance to hear personal stories from moderators and other attendees, as well as share your own experiences in a close-knit, non-judgmental environment.

🥰 Snacks and drinks will be provided. Hope to see you there 🙂

📆 Date: Saturday, December 3rd, 6-8 PM at the SLC Multipurpose Room

📝 Sign up for Afterhours through the form at the link, or feel free to simply drop in!

👉 The link to google form: https://forms.gle/cmNo2tbcof2UvEQ5A                   