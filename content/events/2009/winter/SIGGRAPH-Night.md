---
name: 'SIGGRAPH Night'
short: 'Come out and watch the SIGGRAPH (Special Interest Group on Graphics) conference video review. A video of insane, amazing, and mind blowing computer graphics. .'
startDate: 'March 05 2009 16:30'
online: false
location: 'Comfy Lounge'
---

The ACM SIGGRAPH (Special Interest Group on Graphics) hosts a conference yearly in which the latest and greatest in computer graphics premier. They record video and as a result produce a very nice Video Review of the conference. Come join us watching these videos, as well as a few professors from the UW Computer Graphics Lab. There will be some kind of food and drink, and its guranteed to be dazzling.

