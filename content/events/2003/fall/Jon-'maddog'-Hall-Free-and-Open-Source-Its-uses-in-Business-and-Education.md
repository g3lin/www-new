---
name: 'Jon ''maddog'' Hall: Free and Open Source: Its uses in Business and Education'
short: ' Free and Open Source software has been around for a long time, even longer then shrink-wrapped code.'
startDate: 'December 01 2003 19:00'
online: false
location: 'RCH 101'
---

Free and Open Source software has been around for a long time, even longer then shrink-wrapped code. It has a long and noble history in the annals of education. Even more than ever, due to the drop of hardware prices and the increase of worldwide communications, Free and Open Source can open new avenues of teaching and doing research, not only in computer science, but in other university fields as well.

Learn how Linux as an operating system can run on anything from a PDA to a supercomputer, and how Linux is reducing the cost of computing dramatically as the fastest growing operating system in the world. Learn how other Free and Open Source projects, such as office suites, audio and video editing and playing software, relational databases, etc. are created and are freely available.

[Map and directions](<http://www.csclub.uwaterloo.ca/~cpbell/>)

### Speaker's Biography

Jon "maddog" Hall is the Executive Director of [Linux International](<http://www.li.org/>), a non-profit association of computer vendors who wish to support and promote the Linux Operating System. During his career which spans over thirty years, Mr. Hall has been a programmer, systems designer, systems administrator, product manager, technical marketing manager and educator. He has worked for such companies as Western Electric Corporation, Aetna Life and Casualty, Bell Laboratories, Digital Equipment Corporation, VA Linux Systems, and is currently funded by SGI.

He has taught at Hartford State Technical College, Merrimack College and Daniel Webster College. He still likes talking to students over pizza and beer (the pizza can be optional).

Mr. Hall is the author of numerous magazine and newspaper articles, many presentations and one book, "Linux for Dummies".

Mr. Hall serves on the boards of several companies, and several non-profit organizations, including the USENIX Association.

Mr. Hall has traveled the world speaking on the benefits of Open Source Software, and received his BS in Commerce and Engineering from Drexel University, and his MSCS from RPI in Troy, New York.

In his spare time maddog is working on his retirement project:

<center>maddog's monastery for microcomputing and microbrewing</center>

