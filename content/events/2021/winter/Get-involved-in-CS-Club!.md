---
name: 'Get involved in CS Club!'
short: 'Learn about how you can involved with CS Club this term.'
startDate: 'January 11 2021 19:00'
online: true
location: 'Online'
---

Do you want to take part in CS Club's exciting upcoming events? Want to help out with our website redesign? Come join us on January 11th at 7pm EST on Twitch to learn about how you can become a member of CS Club, the roles that you can take on and how to participate in existing and brand-new community initiatives.

The event will be streamed at [twitch.tv/uwcsclub](<https://twitch.tv/uwcsclub>)

Register at [https://forms.gle/WBGPkvs5HzX1CEj98](<https://forms.gle/WBGPkvs5HzX1CEj98>)

