---
name: 'Rapid prototyping and mathematical art'
short: 'A talk by Craig S. Kaplan.'
startDate: 'April 02 2009 16:30'
online: false
location: 'DC1302'
---

The combination of computer graphics, geometry, and rapid prototyping technology has created a wide range of exciting opportunities for using the computer as a medium for creative expression. In this talk, I will describe the most popular technologies for computer-aided manufacturing, discuss applications of these devices in art and design, and survey the work of contemporary artists working in the area (with a focus on mathematical art). The talk will be primarily non-technical, but I will mention some of the mathematical and computational techniques that come into play.

