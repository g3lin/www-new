---
name: 'Results of Fall 2015 Elections'
short: 'The Computer Science Club has elected its executive for the term, and a new Office Manager and System Administrator have been appointed.See inside for results.'
startDate: 'September 22 2015 21:00'
online: false
location: 'MC 3001'
---

The Computer Science Club has elected its executive for the term, and a new Office Manager and System Administrator have been appointed. The quorum for elections had been reached, and voting members of the CSC voted for their President, Vice President, Treasurer, and Secretary from among many qualified candidates. The new elected executive then proceeded to appoint a System Administrator (who became part of the executive *ex officio*) and an Office Manager. The appointment of a Librarian was delayed because no suitable and willing candidate was found.

The results of the elections are:

- Simone Hu - President
- Theo Belaire - Vice President
- Jordan Upiter - Treasurer
- Daniel Marin - Secretary
- Jordan Pryde - System Administrator
- Office Manager - Ilia Chtcherbakov

<!-- -->

