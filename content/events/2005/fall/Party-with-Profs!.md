---
name: 'Party with Profs!'
short: 'Get to know your profs and be the envy of your friends!'
startDate: 'October 17 2005 17:30'
online: false
location: 'Fishbowl'
---

Come out and meet your professors!! This is a great opportunity to meet professors for Undergraduate Research jobs or to find out who you might have for future courses. One and all are welcome!

And best of all... free food!!!

