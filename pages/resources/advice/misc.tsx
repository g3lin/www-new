import React from "react";

import { Title } from "@/components/Title";

import Content from "../../../content/resources/advice/misc-advice.md";

import { Advice } from "./co-op";

export default function MiscAdvice() {
  return (
    <>
      <Title>Additional Advice</Title>
      <Advice>
        <Content />
      </Advice>
    </>
  );
}
