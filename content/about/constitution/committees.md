---
title: Committees
---

## Programme Committee

1. The Programme Committee shall be a standing committee chaired by the Vice-President.
2. The Vice-President shall appoint and remove members to and from the Programme Committee as needed.
3. The Programme Committee shall plan and arrange the events of the Club.
4. The Programme Committee shall be responsible to the Executive Council and to the Vice-President.

## Systems Committee

1. The Systems Committee (syscom) shall be a standing committee, chaired by the Systems Administrator (sysadmin).
2. New members to the Systems Committee shall be appointed at the Systems Administrator’s discretion. Members should only be appointed to the Systems Committee if they show interest and some existing ability in systems administration.
3. Members should only be removed from the Systems Committee with cause, or when they no longer show interest in systems administration.
4. When a member is added to or removed from the Systems Committee, the Systems Committee and the Executive must be notified via both mailing lists.
5. The Systems Committee will collectively, under the leadership of the Systems Administrator,
    1. operate any and all equipment in the possession of the Club.
    1. maintain and upgrade the software on equipment that is operated by the Club.
    1. facilitate the use of equipment that is operated by the Club.
6. Members of the Systems Committee shall have root access to the machines operated by the Club.

## Web Committee

1. The Web Committee (webcom) will be a standing committee, chaired by the Webmaster.
2. The Webmaster shall appoint and remove members to and from the Web Committee as needed.
3. The Web Committee shall maintain and develop the club website with infrastructure support from the Systems Committee, if necessary.


## Other Committees

1. The President, with approval of the Executive Council, may appoint such special committees as are deemed necessary.
