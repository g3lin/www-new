import path from "path";

import {
  createReadAllPage,
  Options,
} from "@/components/OrganizedContent/ReadAll";
import { createReadAllGetStaticProps } from "@/components/OrganizedContent/static";

export const options: Options = {
  title: "Code of Conduct",
  image: "images/code-of-conduct.svg",
  pagePath: path.join("about", "code-of-conduct"),
};

export default createReadAllPage(options);

export const getStaticProps = createReadAllGetStaticProps(options.pagePath);
