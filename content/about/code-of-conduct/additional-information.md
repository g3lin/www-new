---
title: Additional Information
---

Additionally, the Executive Council are available to help Club members engage with local law enforcement or to otherwise help those experiencing unacceptable behaviour feel safe. In the context of in-person events, organizers will also provide escorts as desired by the person experiencing distress.

Changes to the Code of Conduct are governed by the Club's [constitution](/about/constitution).
