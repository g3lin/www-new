---
author: 'omsmith'
date: 'January 17 2013 00:00'
---

Elections for Winter 2013 have concluded. The following people were elected.

- President: a2brenna
- Vice-president: m4burns
- Treasurer: jsmumfor
- Secretary: mgolechn

<!-- -->

Additionally, the following people were appointed.

- Sysadmin: sharvey
- Office Manager: b2coutts
- Librarian: mimcpher

<!-- -->