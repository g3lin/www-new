---
name: 'Code Party'
short: 'Come to our second code party of the term! You can chill out, work on side-projects 💻, or finish up on homework and assignments 📚!'
startDate: 'November 15 2022 18:00'
endDate: 'November 15 2022 20:00'
online: false
location: 'STC 0020'
poster: 'images/events/2022/fall/Code-Party-2.png'

---
🎉 Code Party is back!

🍵 Come join us and work on assignments or side projects, or just hang out with your CSC friends!

🍩 You can also enjoy free snacks at our popup cafe!

🗓 Event date: November 15th at 6 - 8PM

📍Location: STC 0020

Registration is not required for this event. Hope you see you there!
