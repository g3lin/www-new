---
name: 'Feminism in STEM - a 101 Panel'
short: 'An introductory feminism in STEM panel, free food.'
startDate: 'October 18 2016 17:30'
online: false
location: 'QNC 1507'
---

The CS Club is hosting an introductory panel for applications and benefits of feminism in STEM. Example topics will include the differences between general feminism and feminism applied to STEM. Dr. Prabhakar Ragde from SCS, Swetha Kulandaivelan, and Filzah Nasir will be speaking on the panel. Fatema Boxwala will be moderating. Free food will be there and we're in a fancy room. Come on out!

