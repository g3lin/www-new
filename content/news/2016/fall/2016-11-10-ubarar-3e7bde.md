---
author: 'ubarar'
date: 'November 10 2016 00:00'
---

The Code of Conduct and the amended version can be found below:

- [Proposed CoC](<https://www.csclub.uwaterloo.ca/~exec/proposed-amendment/about/code-of-conduct>)
- [Diff between current and proposed CoC](<https://www.csclub.uwaterloo.ca/~exec/proposed-amendment.patch>)

<!-- -->