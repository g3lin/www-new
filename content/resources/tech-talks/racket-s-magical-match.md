---
index: 15
title: 'Racket''s Magical Match'
presentors:
  - Theo Belaire
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/tbelaire_racket-thumb-small.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/tbelaire_racket.mp4'
    type: 'Talk (x264)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/tbelaire_racket-slides.pdf'
    type: 'Talk (PDF)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/tbelaire_racket-slides-source.rkt'
    type: 'Talk (Rkt)'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/tbelaire_racket-stdlib.rkt'
    type: 'Source (Rkt)'
---

Come learn how to use the power of the Racket match construct to make your code easier to read, less bug-prone and overall more awesome!

Theo Belaire, a fourth-year CS student, will show you the basics of how this amazing function works, and help you get your feet wet with some code examples and advanced use cases.

If you're interested in knowing about the more powerful features of Racket, then this is the talk for you! The material covered is especially useful for students in CS 241 who are writing their compiler in Racket, or are just curious about what that might look like.
