---
index: 1
title: 'Unix 102 Spring 2017'
presentors:
  - Fatema
  - Charlie
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/unix102-s17-thumb-small.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/unix102-s17.mp4'
    type: 'Unix 102 Spring 2017 (mp4)'
---

Finished the bash unit in CS246 and still don't see what's great about Unix? Want to gain some more in-depth knowledge, or some less well-known tips and tricks for using the command line? Unix 102 is the event for you! Fatema is "kind of successful" and "knows things about Unix" and you can be too! Topics covered will be: users, groups and permissions, ez string manipulation, additional skills, tips and tricks.
