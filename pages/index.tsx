import { GetStaticProps } from "next";
import { MDXRemote } from "next-mdx-remote";
import React from "react";

import { ConnectWithUs } from "@/components/ConnectWithUs";
import { DefaultLayout } from "@/components/DefaultLayout";
import { EmailSignup } from "@/components/EmailSignup";
import { EventDescriptionCard } from "@/components/EventDescriptionCard";
import { Image } from "@/components/Image";
import { Link } from "@/components/Link";
import { NewsCard } from "@/components/NewsCard";
import { GetShapesConfig } from "@/components/ShapesBackground";
import { SocialLinks } from "@/components/SocialLinks";
import { Title } from "@/components/Title";
import { Event, getUpcomingEvents } from "@/lib/events";
import { News, getRecentNews } from "@/lib/news";

import styles from "./index.module.css";

interface Props {
  moreEvents: boolean; // true if there are more than 2 upcoming events
  events: Event[]; // array of 0 - 2 events
  news: News[]; // array of 3 news items
}

export default function Home(props: Props) {
  return (
    <>
      <Title>{[]}</Title>
      <DefaultLayout>
        <header className={styles.intro}>
          <div className={styles.introTop} />
          <div className={styles.introContent}>
            <div className={styles.clubTitleWrapper}>
              <Image src="/images/logo-icon.svg" alt="CSC Logo" />
              <h1 className={styles.clubTitle}>Computer Science Club</h1>
            </div>
            <p className={styles.clubDescription}>
              University of Waterloo&apos;s <br />
              student computing community
            </p>
            <SocialLinks color="gradient" size="big" />
          </div>
          <div className={styles.introBottom} />
          <Image
            className={styles.codey}
            src="/images/home/codey_sitting.svg"
            alt="CSC mascot Codey, a blue shiba with circular glasses"
          />
        </header>
      </DefaultLayout>
      <div className={styles.cardsBackground}>
        <div className={styles.cards}>
          <section className={styles.events}>
            <h1 className={styles.cardsHeading}>Upcoming Events</h1>
            <p className={styles.cardsDescription}>
              See past events <Link href="/events/archive">here</Link>
            </p>
            <hr className={styles.cardsDividingLine} />
            {props.events.length === 0 ? (
              <p>
                There are no upcoming events right now. Please check back later!
              </p>
            ) : null}
            <div className={styles.eventCards}>
              {props.events.length > 0
                ? props.events.map((event) => (
                    <EventDescriptionCard
                      {...event.metadata}
                      startDate={new Date(event.metadata.startDate)}
                      endDate={
                        event.metadata.endDate
                          ? new Date(event.metadata.endDate)
                          : undefined
                      }
                      key={
                        event.metadata.name +
                        event.metadata.startDate.toString()
                      }
                    />
                  ))
                : null}
            </div>
            {props.moreEvents ? (
              <p>
                See more upcoming events <Link href="/events">here</Link>
              </p>
            ) : null}
          </section>
          <section className={styles.news} id="news">
            <h1 className={styles.cardsHeading}>News</h1>
            <p className={styles.cardsDescription}>
              Updates from our execs! <br />
              See past news <Link href="/news/archive">here</Link>
            </p>
            <hr className={styles.cardsDividingLine} />
            {props.news.length > 0
              ? props.news.map((news, idx) => (
                  <NewsCard
                    {...news.metadata}
                    date={new Date(news.metadata.date)}
                    key={`${news.metadata.date.toString()}${idx}`}
                  >
                    <MDXRemote {...news.content} />
                  </NewsCard>
                ))
              : null}
          </section>
        </div>
      </div>
      <DefaultLayout>
        <ConnectWithUs />
        <EmailSignup />
      </DefaultLayout>
    </>
  );
}

Home.Layout = function HomeLayout(props: { children: React.ReactNode }) {
  return <div className={styles.page}>{props.children}</div>;
};

export const getStaticProps: GetStaticProps<Props> = async () => {
  const upcomingEvents = await getUpcomingEvents();
  const recentNews = await getRecentNews();
  return {
    props: {
      moreEvents: upcomingEvents.length > 2,
      events: upcomingEvents.slice(0, 2),
      news: recentNews.slice(0, 3),
    },
  };
};

Home.getShapesConfig = (() => {
  const bigDesktopConfig = {
    dots: [
      {
        top: "calc(0.06 * (580rem / 0.65) / 16)",
        right: "90vw",
        width: "calc(168rem / 16)",
        height: "calc(204rem / 16)",
        filter: "var(--teal)",
        opacity: "25%",
      },
    ],
    hash: [
      {
        top: "calc(0.32 * (580rem / 0.65) / 16)",
        left: "12vw",
        width: "calc(60rem / 16)",
        height: "calc(60rem / 16)",
      },
      {
        top: "calc(0.25 * (580rem / 0.65) / 16)",
        right: "9vw",
        width: "calc(60rem / 16)",
        height: "calc(60rem / 16)",
      },
    ],
    plus: [
      {
        top: "calc(0.42 * (580rem / 0.65) / 16)",
        right: "22vw",
        width: "calc(48rem / 16)",
        height: "calc(48rem / 16)",
      },
    ],
    triangle: [
      {
        top: "calc(0.05 * (580rem / 0.65) / 16)",
        left: "20vw",
        width: "calc(68rem / 16)",
        height: "calc(68rem / 16)",
        transform: "rotate(18deg)",
        filter: "var(--teal)",
        opacity: "30%",
      },
      {
        top: "calc(0.02 * (580rem / 0.65) / 16)",
        right: "40vw",
        width: "calc(68rem / 16)",
        height: "calc(68rem / 16)",
        transform: "rotate(-26deg)",
      },
    ],
    waves: [
      {
        top: "calc(0.5 * (580rem / 0.65) / 16)",
        left: "24vw",
        width: "calc(116rem / 16)",
        height: "calc(58rem / 16)",
        filter: "var(--teal)",
      },
      {
        top: "calc(0.1 * (580rem / 0.65) / 16)",
        right: "18vw",
        width: "calc(102rem / 16)",
        height: "calc(50rem / 16)",
        filter: "var(--teal)",
      },
    ],
  };

  const mediumDesktopConfig = {
    dots: [{ ...bigDesktopConfig["dots"][0], top: "6vh" }],
    hash: [
      { ...bigDesktopConfig["hash"][0], top: "32vh" },
      { ...bigDesktopConfig["hash"][1], top: "25vh" },
    ],
    plus: [{ ...bigDesktopConfig["plus"][0], top: "42vh" }],
    triangle: [
      { ...bigDesktopConfig["triangle"][0], top: "5vh" },
      { ...bigDesktopConfig["triangle"][1], top: "2vh" },
    ],
    waves: [
      { ...bigDesktopConfig["waves"][0], top: "50vh" },
      { ...bigDesktopConfig["waves"][1], top: "10vh" },
    ],
  };

  const smallDesktopConfig = {
    dots: [
      {
        ...bigDesktopConfig["dots"][0],
        top: "calc(0.06 * (420rem / 0.65) / 16)",
      },
    ],
    hash: [
      {
        ...bigDesktopConfig["hash"][0],
        top: "calc(0.32 * (420rem / 0.65) / 16)",
      },
      {
        ...bigDesktopConfig["hash"][1],
        top: "calc(0.25 * (420rem / 0.65) / 16)",
      },
    ],
    plus: [
      {
        ...bigDesktopConfig["plus"][0],
        top: "calc(0.42 * (420rem / 0.65) / 16)",
      },
    ],
    triangle: [
      {
        ...bigDesktopConfig["triangle"][0],
        top: "calc(0.05 * (420rem / 0.65) / 16)",
      },
      {
        ...bigDesktopConfig["triangle"][1],
        top: "calc(0.02 * (420rem / 0.65) / 16)",
      },
    ],
    waves: [
      {
        ...bigDesktopConfig["waves"][0],
        top: "calc(0.5 * (420rem / 0.65) / 16)",
      },
      {
        ...bigDesktopConfig["waves"][1],
        top: "calc(0.1 * (420rem / 0.65) / 16)",
      },
    ],
  };

  const mobileConfig = {
    dots: [
      {
        top: "0",
        right: "80vw",
        width: "calc(168rem / 16)",
        height: "calc(152rem / 16)",
      },
    ],
    hash: [
      {
        top: "calc(250rem / 16)",
        right: "80vw",
        width: "calc(45rem / 16)",
        height: "calc(45rem / 16)",
      },
    ],
    triangle: [
      {
        top: "calc(4rem / 16)",
        right: "3vw",
        width: "calc(45rem / 16)",
        height: "calc(45rem / 16)",
        transform: "rotate(16deg)",
      },
    ],
    waves: [
      {
        top: "calc(230rem / 16)",
        left: "86vw",
        width: "calc(102rem / 16)",
        height: "calc(50rem / 16)",
      },
    ],
  };

  if (window.innerWidth <= 768) {
    return mobileConfig;
  } else if (window.innerHeight <= 420 / 0.65) {
    return smallDesktopConfig;
  } else if (window.innerHeight <= 580 / 0.65) {
    return mediumDesktopConfig;
  }
  return bigDesktopConfig;
}) as GetShapesConfig;
