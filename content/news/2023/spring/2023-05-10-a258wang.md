---
author: 'a258wang'
date: 'May 10 2023 00:00'
---

The Spring 2023 Elections were held Wednesday, May 10th at 6PM EDT. Here are the elected executives for the term!

- President: Sat Arora (s97arora)
- Vice-President: Joshua Duho Kim (j649kim)
- Assistant Vice-President: Andrea Ma (a49ma)
- Treasurer: Amy Wang (a258wang)
- Sysadmin: Raymond Li (r389li)
