---
name: 'End of Term Party'
short: 'Come celebrate the end of the term with us in MC Comfy!'
startDate: 'July 24 2017 18:00'
online: false
location: 'MC Comfy'
---

Come celebrate the end of the term with us in MC Comfy! We will be serving Urban Bricks!

