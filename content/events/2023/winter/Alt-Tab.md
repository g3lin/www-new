---
name: 'Alt Tab - Student Ted talks'
short: 'Join CSC for Alt-Tab, a lightning tech talk series presented by students! '
startDate: 'March 14 2023 19:00'
endDate: 'March 14 2023 21:00'
online: false
location: 'MC 2054'
poster: 'images/events/2023/winter/Alt-Tab.png'
---

🎙️ Join CSC for Alt-Tab, a lightning tech talk series presented by students! Alt-Tab consists of several ~15-minute talks about a variety of topics related to computer science and technology. Snacks will only be available to CSC members in accordance with MathSoc regulations.

⚡ Talk list:

- Navya Mehta (4B, CFM): Performant Deep Learning Inference at Scale: Challenges and Case Studies at WOMBO AI
- Andrew Dong (2B, CS): AlphaGo and Artificial Intuition
- Daniel Matlin (4A, CS): Google Sheets and Giggles
- Joshua Liu (2B, CS): 101 Ways to escape (Python) jail
- Felix Yang (3B, CS): Pairing Functions
- Kavin Satheeskumar (3A, CS): State Estimation
- GT (3A, CS): The Most Robust Design Pattern

📅 Date: March 14th, 2023 at 7-9 PM EST

📍 Location: MC 2054
