---
index: 42
title: 'Why you should care about functional programming with Haskell'
presentors:
  - Andrei Barbu
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/abarbu1-thumb-small.jpg'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/abarbu1-thumb-large.jpg'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/abarbu1.avi'
    type: 'XviD'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/abarbu1.ogg'
    type: 'Ogg/Theora'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/abarbu1.mp4'
    type: 'MP4'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/abarbu1.mpg'
    type: 'MPG'
---

TODO
