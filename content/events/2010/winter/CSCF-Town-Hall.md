---
name: 'CSCF Town Hall'
short: 'Come to a town hall style meeting with the managers of CSCF to discuss how to improve the undergraduate (student.cs) computing environment. Have gripes? Suggestions? Requests? Now is the time to voice them.'
startDate: 'February 25 2010 16:30'
online: false
location: 'DC1302'
---

Come to a town hall style meeting with the managers of CSCF to discuss how to improve the undergraduate (student.cs) computing environment. Have gripes? Suggestions? Requests? Now is the time to voice them.

CSCF management (Bill Ince, Associate Director; Dave Gawley, Infrastructure Support; Dawn Keenan, User Support; Lawrence Folland, Research Support) will be at the meeting to listen to student concerns and suggestions. Information gathered from the meeting will be summarized and taken to the CSCF advisory committee for discussion and planning.

