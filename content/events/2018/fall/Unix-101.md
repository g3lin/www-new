---
name: 'Unix 101'
short: 'Learn the basics of UNIX with the CS Club on October 22, 5:30 PM in MC 3003.'
startDate: 'October 22 2018 17:30'
online: false
location: 'MC-3003'
---

Interested in Unix, but don’t know where to start? Then come learn some basic topics with us including interaction with the command line, motivation for using it, some simple commands, and more. This event targets first years with minimal Unix experience.

Light refreshments and snacks will be provided.

