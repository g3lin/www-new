---
name: 'Another Party of Code'
short: 'There is a CSC Code Party Friday starting at 7:00PM (1900) until we get bored (likely in the early in morning). Come out for fun hacking times, spreading Intertube memes (optional), hacking on the OpenMoko, creating music mixes, and other general classiness. There will be free energy drinks for everyone''s enjoyment.'
startDate: 'March 19 2010 19:00'
online: false
location: 'Comfy Lounge'
---

There is a CSC Code Party Friday starting at 7:00PM (1900) until we get bored (likely in the early in morning). Come out for fun hacking times, spreading Intertube memes (optional), hacking on the OpenMoko, creating music mixes, and other general classiness. There will be free energy drinks for everyone's enjoyment.

