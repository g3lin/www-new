---
name: 'CSClub Elections'
short: 'Elections are scheduled for Tues, Sep 16 @ 4:30 pm in the comfy lounge. The nomination period closes on Mon, Sep 15 @ 4:30 pm. Nominations may be sent to cro@csclub.uwaterloo.ca. Candidates should not engage in campaigning after the nomination period has closed.'
startDate: 'September 16 2008 16:30'
online: false
location: 'Comfy Lounge'
---

Elections are scheduled for Tues, Sep 16 @ 4:30 pm in the comfy lounge. The nomination period closes on Mon, Sep 15 @ 4:30 pm. Nominations may be sent to cro@csclub.uwaterloo.ca. Candidates should not engage in campaigning after the nomination period has closed.

