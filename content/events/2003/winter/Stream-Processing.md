---
name: 'Stream Processing'
short: 'A talk by Assistant Professor Michael McCool'
startDate: 'March 25 2003 16:30'
online: false
location: 'MC2065'
---

Stream processing is an enhanced version of SIMD processing that permits efficient execution of conditionals and iteration. Stream processors have many similarities to GPUs, and a hardware prototype, the Imagine processor, has been used to implement both OpenGL and Renderman.

It is possible that GPUs will acquire certain properties of stream processors in the future, which should make them easier to use and more efficient for general-purpose computation that includes data-dependent iteration and conditionals.

