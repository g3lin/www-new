---
name: 'KW Perlmongers Talk'
short: '*by Justin Wheeler.*In his own words, this talk will cover the virtues of Perl: CPAN, Moose, CPAN, Catalyst, CPAN, DBIx::Class, CPAN, TMTOWTDI, and did I mention CPAN?'
startDate: 'November 15 2012 19:00'
online: false
location: 'DC 1302'
---

In his own words, this talk will cover the virtues of Perl: CPAN, Moose, CPAN, Catalyst, CPAN, DBIx::Class, CPAN, TMTOWTDI, and did I mention CPAN?

If you've never used Perl before, don't be scared away by the jargon—the talk should be accessible to all CS students, and even if you find it hard to follow, we will be serving pizza!

