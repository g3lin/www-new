---
author: 'ztseguin'
date: 'August 31 2018 01:00'
---

While power has been restored to the Math & Computer Building (MC), power is not yet available in the machine room. As a result, most CSC services are still unavailable at this time.

The current estimate for power is Saturday, Sept. 1.

Currently, due to machines located in other buildings and emergency generator power to select machines in MC, the following services are currently available:

- [Mirror](<http://mirror.csclub.uwaterloo.ca>)
- Mail ([Webmail](<https://mail.csclub.uwaterloo.ca>), SMTP and IMAP
- Caffeine (including web hosting)

<!-- -->

Once power is restored to the machine room, the Systems Committee will continue restoring remaining services.