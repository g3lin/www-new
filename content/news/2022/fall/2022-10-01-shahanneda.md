---
author: 'shahanneda'
date: 'October 1 2022 00:00'
---
UW DSC, UW CSC, and Laurier CS are excited to bring back the Project Program event for Fall 2022.

As you may recall, Project Program is a month-long experience where we support you as you create your own project! 🖥

We will once again be pairing groups of mentees with mentors to start and complete side projects. 

🏆 Together, you will brainstorm project ideas, create roadmaps with milestones and achievements, and in the end you’ll have the opportunity to present your project to win prizes! 

Applications for mentors and mentees have officially opened. As a mentee, you’re responsible for creating a project, collaborating with your group, and learning along the way. As a mentor, you’ll be tasked with helping teams of students and ensuring everyone stays on track. ✨

If you’re interested in either being a mentee or mentor, sign up today! 


👉  Sign up for the Mentee Applications at https://forms.gle/iUFTVYiV8k7R7QA98
👉  Sign up for the Mentor Applications at https://forms.gle/XKPGjBPSBvSA2tc68

Alternatively, you can email us at exec@csclub.uwaterloo.ca with a short description about yourself, your experience, as well as what technical fields you’re comfortable mentoring.

📅 Deadline to Sign Up: October 7th, 2022 11:59 PM EST

Interested in learning about Project Program but missed the info session? No worries! Come check out the slideshow recapping important information for our event!

👉  Kick-off Event Slides: https://docs.google.com/presentation/d/1SSbm6MyUxogv0XPdle2H1dtQd_iVhxVf5XvLuqa20Ec/edit?usp=sharing!
👉  Kick-off Feedback Form: https://forms.gle/T3J8uneBaETdHdaZ7 
