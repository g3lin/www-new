---
name: 'Recovering From a Rough Term'
short: 'Join us to learn about other students have bounced back from a rough term, and how you can too.'
startDate: 'January 28 2021 19:00'
online: true
location: 'Online'
---

The fall term has ended and the winter term is ramping up. Maybe you didn't meet all your expectations last term, you are feeling overwhelmed with your coop search, or you are feeling stuck with a bad mindset-we've been there before and you're not alone.

Introducing the first session of our Afterhours series, join us for a chat with fellow UW CS students. Learn about how they bounced back from their rough times and how you can too!

-  Where: Zoom
-  When: January 28th 7 - 8 pm EST
-  Sign up: [https://forms.gle/MnKmGcXeazwKhTd7A](<https://forms.gle/MnKmGcXeazwKhTd7A>)

<!-- -->

