---
name: 'Programming Contest'
short: 'Come out, program, and win shiny things!'
startDate: 'November 29 2005 17:30'
online: false
location: 'TBA'
---

The Computer Science club is holding a programming contest open to all students on Tuesday the 29th of November at 5:30PM. C++,C,Perl,Scheme\* are allowed. Prizes totalling in value of $75 will be distributed.

And best of all... free food!!!

