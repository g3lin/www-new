---
name: 'Wilderness Programming'
short: 'Paul Lutus describes his early Apple II software development days, conducted from the far end of a 1200-foot power cord, in a tiny Oregon cabin. Paul describes how he wrote a best-seller (Apple Writer) in assembly language, while dealing with power outages, lightning storms and the occasional curious bear.'
startDate: 'January 18 2010 15:30'
online: false
location: 'MC2066'
---

Paul Lutus describes his early Apple II software development days, conducted from the far end of a 1200-foot power cord, in a tiny Oregon cabin. Paul describes how he wrote a best-seller (Apple Writer) in assembly language, while dealing with power outages, lightning storms and the occasional curious bear.

Paul also describes his subsequent four-year solo around-the-world sail in a 31-foot boat. And be ready with your inquiries -- Paul will answer your questions.

Paul Lutus has a wide background in science and technology. He designed spacecraft components for the NASA Space Shuttle and created a mathematical model of the solar system used during the Viking Mars lander program. Then, at the beginning of the personal computer revolution, Lutus switched career paths and took up computer science. His best-known program is "Apple Writer," an internationally successful word processing program for the early Apple computers.

