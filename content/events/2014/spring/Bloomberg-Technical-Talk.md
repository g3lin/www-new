---
name: 'Bloomberg Technical Talk'
short: 'Learn how functional programming is used in the real world, while enjoying free dinner, and free swag.'
startDate: 'June 19 2014 17:30'
online: false
location: 'MC 4064'
---

Enjoy a free dinner while Max Ransan, a lead developer at Bloomberg, talks about the use of functional programming within a recently developed product from Bloomberg. This includes UI generation, domain-specific languages, and more! Free swag will also be provided.

