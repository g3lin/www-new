import React, { ReactNode } from "react";

import { Image } from "./Image";
import { Link } from "./Link";

import styles from "./TechTalkCard.module.css";

interface DownloadLink {
  file: string;
  type: string;
  size?: string;
}

interface TechTalkProps {
  title: string;
  presentors: string[];
  poster: string;
  links: DownloadLink[];
  children: ReactNode;
}

export function TechTalkCard({
  title,
  poster,
  presentors,
  links,
  children,
}: TechTalkProps) {
  return (
    <article className={styles.card}>
      <aside>
        <Image
          alt={`Thumbnail of tech talk by ${presentors.join(", ")}: ${title}`}
          src={poster}
        />
      </aside>
      <section className={styles.content}>
        <h1>{title}</h1>
        <div>{children}</div>

        <h2>Download:</h2>
        <ul>
          {links.map((link) => (
            <li key={link.file + link.type}>
              <DownloadLink {...link} />
            </li>
          ))}
        </ul>
      </section>
    </article>
  );
}

function DownloadLink({ file, type, size }: DownloadLink) {
  const text = size ? `${type} (${size})` : type;
  return <Link href={file}>{text}</Link>;
}
