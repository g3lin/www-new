## Academic Advice

#### How can I create study groups without risking academic infidelity?

Find a nice group of people that you study well with and meet every once in a while to work on things together, you can do that generally by asking around via messaging platforms/office hours. To avoid plagiarism, avoid discussing intricate details of the solution but rather bounce ideas off one another, and leave yourself 30 minutes after the meeting before you write up a solution.

#### How can I make sure that I understand the course content?

Try to complete your assignments without consulting your notes. It will be very challenging to do if you are not very confident in the content and is a good indicator that you need to understand the content better! Try to review it again and do the assignment without referring back to it.

#### What can I do when I feel unmotivated?

Try to manage your pace when it comes to work. It’s really easy to burn out and lose motivation in the middle to end of the term, when you need it the most. Give yourself a breather and take breaks!

#### How can I enjoy the school term when it becomes stressful?

Assignments can be pretty endless, so make sure you celebrate your small wins. Modularize your tasks and reflect on all the work you’ve done over a period of time. It’s often much more than you think.

#### Who can I go to when I have an issue regarding a course?

If you have issues regarding courses, there are MathSoc class representatives who can help voice your concerns to involved faculty members.
