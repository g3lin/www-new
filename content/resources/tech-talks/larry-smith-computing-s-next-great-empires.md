---
index: 61
title: 'Larry Smith: Computing''s Next Great Empires'
presentors:
  - Larry Smith
thumbnails:
  small: 'http://mirror.csclub.uwaterloo.ca/csclub/audio-file.png'
  large: 'http://mirror.csclub.uwaterloo.ca/csclub/audio-file.png'
links:
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/larry-smith-talk.ogg'
    type: 'Ogg'
  - file: 'http://mirror.csclub.uwaterloo.ca/csclub/larry-smith-talk.mp3'
    type: 'MP3'
---
