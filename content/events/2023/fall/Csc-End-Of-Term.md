---
name: 'CSC End of Term'
short: "Surprise, it's almost end of term! Come join us for a fun night of winter-themed activities with cool people."
startDate: 'December 01 2023 18:00'
endDate: 'December 01 2023 21:00'
online: false
location: 'STC 0010 & STC 0020'
poster: 'images/events/2023/fall/1707694507458--CSC-EOT.png'
---

🎊 Surprise, it's almost end of term!

🎉 Come join us for a fun night of winter-themed activities with cool people. 

🔥 We will be painting ornaments, having an official christmas tree decorating ceremony, decorating cookies, playing board games, and singing our hearts out with Christmas (and iconic songs) karaoke.

🥳 If you want to have some fun before exams, make sure to attend!

⭐ P.S There will be free snacks and hot chocolate :D

📆 Event Date: Friday, December 1st, 6-9 PM at STC 0010 and 0020

