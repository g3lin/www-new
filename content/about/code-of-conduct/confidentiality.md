---
title: Confidentiality
---

The Club recognizes that all members have a right to privacy, and will handle complaints confidentially.

As such, proceedings will be kept confidential as described below, to the extent allowed by whichever University policy applies to the complaint. Relevant policies include Policy 42 (Prevention and Response to Sexual Violence), Policy 33 (Ethical Behaviour), or Policy 34 (Health, Safety, and Environment).

Information will only be reported to Police Services when directly required to by applicable policy. In such a case, only the required information will be provided, anonymized if possible.
**Information that will be kept in Club records and be available to Officers in the future will be limited to**: the date the complaint was made, the name of the subject of the complaint, the name of the Handling Officer, the decision made on the complaint, the date the decision on the complaint was made, and if applicable, the date of the appeal, the party making the appeal (Complainant or Subject), the decision made on the appeal by the Officers, and the date of the Officers decision on the appeal.

**The information the Handling Officer and Faculty Advisor will jointly keep records of, in addition to the information kept in the Club records, will be limited to** : the name of the complainant, and a summary of the complaint. This information will be available to Officers that are handling future complaints if it is requested and the Handling Officer deems it relevant to the new complaint.
