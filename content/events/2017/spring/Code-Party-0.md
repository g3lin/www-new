---
name: 'Code Party 0'
short: 'Join us for Code Party 0!'
startDate: 'June 22 2017 18:00'
online: false
location: 'STC 0020'
---

Come code with us, eat some food, do some things. Personal projects you want to work on? Homework projects	you need to finish? Or want some time to explore some new technology and chat about it? You can join us at Code Party 0 and do it, with great company and great food.

