import React from "react";

import { Link } from "./Link";
import { SocialLinks } from "./SocialLinks";

import styles from "./ConnectWithUs.module.css";

export function ConnectWithUs() {
  return (
    <section>
      <h1 className={styles.header}>Connect with us!</h1>
      <p>
        Drop by any of our social media outlets to learn more about us, keep
        up-to-date with our upcoming events, or to chat with our members!
      </p>

      <div className={styles.socialLinks}>
        <SocialLinks color="gradient" size="big" />
      </div>

      <p>
        Send feedback through our{" "}
        <Link href="https://bit.ly/uwcsclub-feedback-form">Feedback Form</Link>.
      </p>
    </section>
  );
}
