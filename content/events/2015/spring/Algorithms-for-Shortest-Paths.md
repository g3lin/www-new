---
name: 'Algorithms for Shortest Paths'
short: 'Come to this exciting talk about path-finding algorithms which is being presented by Professor Anna Lubiw.'
startDate: 'July 16 2015 18:00'
online: false
location: 'MC 4064'
---

Finding shortest paths is a problem that comes up in many applications: Google maps, network routing, motion planning, connectivity in social networks, and etc. The domain may be a graph, either explicitly or implicitly represented, or a geometric space.

Professor Lubiw will survey the field, from Dijkstra's foundational algorithm to current results and open problems. There will be lots of pictures and lots of ideas.

[Click here to see the slides from the talk.](<http://mirror.csclub.uwaterloo.ca/csclub/shortest-paths-CSclub.pdf>)

[Click here for the recorded talk.](</media/Algorithms%20for%20Shortest%20Paths>)

