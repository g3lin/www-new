---
author: 'n3parikh'
date: 'October 19 2021 00:00'
---

Fall 2021 elections have concluded. Here are your executives for the term:

- President: Dora Su (d43su)
- VP (Head of Events): Jason Sang (jzsang)
- AVP (Head of Marketing): Anjing Li (a348li)
- Treasurer: Yanni Wang (y3859wan)
- Sysadmin: Max Erenberg (merenber)

<!-- -->

Additionally, the following postions were appointed:

- Head of Discord: Andrew Wang
- Head of Design: Sam Honoridez
- Head of Reps: Juthika Hoque


<!-- -->
